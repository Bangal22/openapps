import sys
import subprocess

FIREFOX = "/Program Files/Mozilla Firefox/firefox.exe"
GOOGLE = "/Program Files/Google/Chrome/Application/chrome.exe"
BRAVE = "/Program Files/BraveSoftware/Brave-Browser/Application/brave.exe"


def getParams(): return sys.argv[1: len(sys.argv)]


def openApp(app):
    if app == "firefox" or app == "f":
        subprocess.run(FIREFOX)
    elif app == "google" or app == "g":
        subprocess.run(GOOGLE)
    elif app == "brave" or app == "b":
        subprocess.run(BRAVE)
    else:
        return False

    return True


def main():
    params = getParams()
    result = openApp(params[0])

    print("This application is not within the established parameters. ") if not result \
        else print("Successful open application")


if __name__ == "__main__":
    main()
